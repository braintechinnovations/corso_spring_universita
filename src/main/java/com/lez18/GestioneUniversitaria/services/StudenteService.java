package com.lez18.GestioneUniversitaria.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lez18.GestioneUniversitaria.models.Studente;

@Service
public class StudenteService {

	@Autowired
	private EntityManager entMan;
	
	private Session getSession() {
		return entMan.unwrap(Session.class);
	}
	
	public Studente insert(Studente objStud) {
		
		Studente temp = new Studente();
		temp.setNome(objStud.getNome());
		temp.setCognome(objStud.getCognome());
		temp.setMatricola(objStud.getMatricola());
		temp.setNascita(objStud.getNascita());
		
		Session sessione = getSession();
		
		try {
			sessione.save(temp);
			
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return null;
		
	}
	
	public List<Studente> findAll(){
		Session sessione = getSession();
		return sessione.createCriteria(Studente.class).list();
	}
	
}

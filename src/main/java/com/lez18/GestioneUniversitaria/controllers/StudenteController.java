package com.lez18.GestioneUniversitaria.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lez18.GestioneUniversitaria.models.Studente;
import com.lez18.GestioneUniversitaria.services.StudenteService;

@RestController
@RequestMapping("/studente")
public class StudenteController {
	
	@Autowired
	private StudenteService service;

	@PostMapping("/insert")
	public Studente inserisci_studente(@RequestBody Studente objStud) {
		return service.insert(objStud);
	}
	
	@GetMapping("/")
	public List<Studente> lista_studenti(){
		return service.findAll();
	}
	
}
